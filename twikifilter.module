<?php
/* $Id */


 /*
 * @file
 * This module adds TWiki-style filters via the hook_filter.
 *
 * copyright Matthew Radcliffe, Kosada Inc.
 *
 */

 /*
 * Display help and module information
 * @param section which sectino of the site we're displaying
 * @return help text for section
 */

 function twikifilter_help($path, $arg)
 {
     switch($path)
     {
         case 'admin/help#twikifilter':
             return '<p>' . t('Adds TWiki standard formatting filter.');
         break;
     }
 } // function twikifilter_help

/*
 function twikifilter_theme($existing, $type, $theme, $path)
 {
     return array(
        'twiki_list' => array($data => NULL, $title => NULL, $attributes => NULL),
     );
 } // function twikifilter_theme
*/
 
 /*
 * filter tips
 * @param delta, format, long
 * @return text
 */
 function twikifilter_filter_tips($delta, $format, $long = FALSE)
 {

     switch($delta)
     {
         case 0:
            if ($long)
            {
                return t('TWiki filter: Allows TWiki-style table, bullet list, numbered list, headings, and separators.');
            }
            else
                return t('TWiki filter: Allows TWiki-style table, bullet list, numbered list, headings, and separators.');
            break;
         case 1:
            if ($long)
            {
                $o = '';
                $o .=  t('TWiki Formatting filter: Allows basic TWiki-style formatting such as bold, italics, code, and strikethrough.') . '<p />';
                $tags = array(
                    'bold' => array(t('bolds a word inside the code'), t('*word*'), t('<b>word</b>') ),
                    'italics' => array( t('italicizes the word inside the code'), t('_word_'), t('<i>word</i>') ),
                    'bold italics' => array(t('bolds and italicizes the word inside the code'), t('__word__'),t('<b><i>word</i></b>')),
                    'code' => array(t('places string of text inside <code></code> tags'), t('=word='), t('<code>word</code>')),
                    'bolded code' => array(t('places bolded string of text inside <code></code> tags'), t('==word=='), t('<code><b>word</b></code>')),
                    'strikethrough' => array(t('strike out the word inside the code'),t('-word-'),t('<strike>word</strike>'))
                    );
                $header = array(
                    '1' => 'Description',
                    '2' => 'TWiki Code',
                    '3' => 'Output' );

                $o .= theme('table',$header,$rows);

            }
            else
                return t('TWiki Formatting filter: Allows basic TWiki-style formatting such as bold, italics, code, and strikethrough.');
            break;
         case 2:
            if ($long)
            {
                $o = '';
                $o .= t('TWiki WikiWord filter: Allows basic TWiki-style WikiWord auto-linking.') . '<p />';
                $o .= t('A WikiWord is the title of a page, story, blog, or other content type.  A WikiWord must be formatted with the first letter of each word as a capital such as WikiWord.  The system will automatically create a link to the topic that you link to.');
                return $o;
            }
            else
                return t('TWiki WikiWord filter: Allows basic TWiki-style WikiWord auto-linking.');
            break;
     }
 } // function twikifilter_filter_tips



 /*
 * Implementation of hook_filter
 * @param op, delta, format, text
 * @return text text to return
 */
 function twikifilter_filter($op, $delta= 0, $format = -1, $text = '') 
 {
     if ($op == 'list')
     {
         return array( 
             0 => t('TWiki filter'),
             1 => t('TWiki Formatting filter'),
             2 => t('TWiki WikiWord filter')
         );
     }

	 if ( arg(0) == 'node' && is_numeric(arg(1)) )
		 $nid = arg(1);

     switch ($delta) // all other operations
     {
         case 0:
             switch ($op)
             {
                 case 'description':
                     return t('Advanced TWiki formatting such as tables, lists, and headings.');
                 case 'prepare':
                     return $text;
                 case 'no cache':
                     return TRUE;
                 case 'process':

                     $page = array();
                    
                     $textarray = preg_split("/\n/",$text);
                     $count = 0;
                     foreach ($textarray as $line) // let's split everything into lines
                     {
                         $attributes = array();
                         if( preg_match("/^(   |\t)+ *[^\s]/",$line) > 0 ) // bullet list
                         {
                             unset($depth);
                             unset($data);
                             if (preg_match("/^(   |\t)+ *[^0-9A-Za-z]/",$line) > 0)
                                 $attributes['type'] = '*';
                             else
                             {
                                 if (preg_match("/^(   |\t)+ *\d/",$line) > 0)
                                     $attributes['type'] = '1';
                                 else if (preg_match("/^(   |\t)+ *[ivxlm]/",$line) > 0)
                                     $attributes['type'] = 'i';
                                 else if (preg_match("/^(   |\t)+ *[IVXLM]/",$line) > 0)
                                     $attributes['type'] = 'I';
                                 else if (preg_match("/^(   |\t)+ *[A-Z]/",$line) > 0)
                                     $attributes['type'] = 'A';
                                 else
                                     $attributes['type'] = 'a';
                             }
                             $depth = preg_match_all("/(   |\t)/",$line, $dummy);
                             $data = preg_replace("/(   |\t)+ *[^\s](.*)/","$2",$line);

                             if ($page[$count]['type'] != 'item-list')
                             {
                                 $count++;
                                 $arrcnt = 0;
                                 $page[$count] = array();
                                 $page[$count]['type'] = 'item-list';
                                 $page[$count][$arrcnt]['data'] = $data;
                                 $page[$count]['attributes']['depth'] = $depth;
                                 $page[$count]['attributes']['type'] = $attributes['type'];
                             }
                             else if ($page[$count]['attributes']['depth'] == $depth)
                             {
                                 $arrcnt++;
                                 $page[$count][$arrcnt]['data'] = $data;
                             }
                             else {
                                 if (!isset($page[$count][$arrcnt]['children']))
                                    $page[$count][$arrcnt]['children'] = array();
                                 $page[$count][$arrcnt]['children'] = _twikifilter_make_child($page[$count][$arrcnt]['children'],$page[$count]['attributes']['depth'],$depth,$data);
                                 
                             }
                         }
                         else if (preg_match("/^\|.*\|/",$line) > 0)
                         {
                             $line = preg_replace("/\|\|/","| |",$line);
                             $tmp = preg_replace("/^\|(.*)\|$/","$1",$line);

                             if ($page[$count]['type'] == 'table')
                             {
                                 $page[$count]['rows'][] = preg_split('/\|/', $tmp);
                             }
                             else 
                             {
                                 $count++;
                                 $page[$count]['type'] = 'table';
                                 $page[$count]['attributes'] = array('id' => 'table-'.$count, 'class' => 'sortable table-'.$count);

                                 if (preg_match("/^\|[*](.*)[*]|/", $line) > 0)
                                     $page[$count]['header'] = preg_split('/\|/', $tmp);
                                 else 
                                     $page[$count]['rows'] = array(preg_split('/\|/', $tmp));
                             }
                         }
                         else if (preg_match("/^---/",$line) > 0)
                         {
                             $count++;
                             if (preg_match("/^---$/",$line) > 0)
                                 $page[$count]['type'] = 'sep';
                             else if (preg_match("/^---[+]{1,}/",$line) > 0)
                             {
                                 $page[$count]['type'] = 'heading';
                                 $page[$count]['level'] = preg_match_all("/[+]/",$line,$dummy);
                                 $page[$count]['title'] = preg_replace("/^---[+]{1,} (.*)/","$1",$line);
                                 $page[$count]['link'] = preg_replace("/ /","_",$page[$count]['title']);
                             }
                         }
                         else 
                         {
                             $count++;
                             $page[$count]['type'] = 'none';
                             $page[$count]['data'] = $line;
                         }
                     }

                     // Theme Section
                     $o = '';
                     /* FIXME: temporary sorttable.js that some dude made */
                     if (file_exists(drupal_get_path('module','twikifilter').'/sorttable.js'))
                        drupal_add_js(drupal_get_path('module','twikifilter').'/sorttable.js', 'module', 'header', NULL, NULL, NULL);
                     foreach ($page as $type)
                     {
                        if ($type['type'] == 'item-list')
                        {
                            if ($type['attributes']['type'] == '*')
                                $btype = 'ul';
                            else
                                $btype = 'ol';
                            $atype = $type['attributes']['type'];
                            unset($type['attributes']);
                            unset($type['type']);
                            _twikifilter_strip_array($type);
                     //       var_dump($type);
                            /* FIXME: We need to make our own theme_twiki_list() function because
                               theme_item_list cannot handle nesting of different types */
                            $o .= theme_item_list($type, NULL, $btype, array('type' => $atype) );
                        }
                        else if ($type['type'] == 'table')
                            $o .= theme('table',$type['header'],$type['rows'],$type['attributes'], NULL);
                        else if ($type['type'] == 'heading')
                            $o .= '<h'.$type['level'].'>'.l($type['title'],'node/'.arg(1),$options = array('fragment' => $type['link'])).'</h'.$type['level'].'>';
                        else if ($type['type'] == 'sep')
                            $o .= '<hr />';
                        else if ($type['type'] == 'none')
                            $o .= preg_replace("/\n/","<br />",$type['data']);
                            
                     }

                     return $o;
                 case 'settings':
                     return $form;
             }
             break;
         case 1:
             switch($op)
             {
                 case 'description':
                     return t('Formats document according to TWiki standard.');
                 case 'prepare':
                     // let us escape < > tags here for when we need to do so... <code> stuff.
                     return $text;
                 case 'no cache':
                     return FALSE;
                 case 'process':

                     $mod = '';
					 $i++;

                     /* Basic formatting */
                     $mod = preg_replace("/[\*]([A-Za-z0-9]+)[\*]/","<b>$1</b>",$text);
                     $mod = preg_replace("/\s+__([A-Za-z0-9]+)__\s+/","<b><i>$1</i></b>",$mod);
                     $mod = preg_replace("/\s+_([A-Za-z0-9]+)_\s+/","<i>$1</i>",$mod);
                     $mod = preg_replace("/\s+==([A-Za-z0-9]+)==\s+/","<code><b>$1</b></code>",$mod);
                     $mod = preg_replace("/\s+=([A-Za-z0-9]+)=\s+/","<code>$1</code>",$mod);
                     $mod = preg_replace("/\s+\-([A-Za-z0-9]+)\-\s+/","<strike>$1</strike>",$mod);
                     $mod = preg_replace("/~([A-Z]+)/","$1",$mod);

                     /* URLs */
					 $urlregex = "[A-Za-z0-9\.\,\%\/\+\:\=\?\-\_]+";
					 $mailregex = "[A-Za-z0-9\-\_\.\@]+";
					 $commonurls = "(http\:\/\/$urlregex|https\:\/\/$urlregex|ftp\:\/\/$urlregex|mailto\:$mailregex)";

					 $mod = preg_replace("/\[\[$commonurls\]\[([\S ]+)\]\]/","<a href=\"$1\" title=\"$2\">$2</a>",$mod);

					return $mod;
                 case 'settings':
                     return $form;
             }
             break;
        case 2:
            switch($op)
            {
                case 'description':
                    return t('linkifys WikiWord links.');
                case 'prepare':
                    $prep = '';
					$urlregex = "[A-Za-z0-9\.\,\%\/\+\:\=\?\-\_]+";
					$mailregex = "[A-Za-z0-9\-\_.\@]+";
					$wwregex = "([A-Z][A-Za-z0-9]+[.])?([A-Z]+[a-z\d]+[A-Z]+[A-Za-z\d]+|[A-Z][A-Z]+[a-z\d]+)";
                    $htmlregex = "<.*>\S+<\/\S+>";
					$i = 0;

					$x = preg_match_all("/(\[\[\S+\]\[\S+\]\])|(http\:\/\/($urlregex)|mailto\:($urlregex)|https\:\/\/($urlregex)|www\.($urlregex))|($htmlregex)/",$text,$urls);
					$prep = preg_replace("/(\[\[\S+\]\[\S+\]\])|(http\:\/\/($urlregex)|mailto\:($urlregx)|https\:\/\/($urlregex)|www\.($urlregex))|($htmlregex)/","%URL%",$text);
                    $prep = preg_replace("/$wwregex/","% $1$2",$prep);
					foreach ($urls[0] as $url)
					{
						if ( preg_match("/\[\[$wwregex\]\[\S+\]\]/",$url) )
						{
							$url = preg_replace("/\[\[$wwregex\]\[(\S+)\]\]/","% $1$2 %$3",$url);
							$prep = preg_replace("/%URL%/","$url",$prep,1);
						}
						else
							$prep = preg_replace("/%URL%/","$url",$prep,1);
						$i++;
					}
                    return $prep;
                case 'no cache':
                    return FALSE;
                case 'process':
                    $mod = '';
					$wwoptions = array(
						'attributes' => array(
							'class' => 'twikifilter-wikiword' ),
						'alias' => FALSE );

					$currweb = _twikifilter_get_node_term($nid); // get the current web 
					$wwregex = "([A-Z][A-Za-z0-9]+[.])?([A-Z]+[a-z\d]+[A-Z]+[A-Za-z\d]+|[A-Z][A-Z]+[a-z\d]+)";

                    $x = preg_match_all("/% $wwregex( %\S+)?/",$text,$wikiwords);

                    if ( $x == 0 ) // let's get out
                        return $text;

                    /* using array element 0 because there's only one match in that regex */
                    $wikiwords = array_unique($wikiwords[0]);  // we only need unique wiki words... was $wikiwords[0]

                    foreach ( $wikiwords as $lt )
                    {
                        if( $mod == '' )
                            $mod = $text;

						$tmp = array();

						$x = preg_match("/% $wwregex( %(\S+))?/",$lt,$tmp);
						if ( $tmp[1] == '' )
							$tmp[1] = $currweb;
						else
							$tmp[1] = rtrim($tmp[1],'.');
                        $i = 0;
                        $lt = preg_replace("/^% /","",$lt); // let's get rid of escape characters
                        $tmp[2] = preg_replace("/'/","\'",$tmp[2]);

						$node = _twikifilter_get_node($tmp[2],$tmp[1]);
						if( $node <> 0 )
                        {
                            $i = 1;
                            /* FIXME : this is probably a bad implementation of paths.module */
							$wwoptions['attributes']['name'] = $node->title;
							$wwoptions['attributes']['class'] = 'wiki';
                            if (isset($tmp[4]))
                                $node->title = $tmp[4];
                            $node->url = l($node->title,'node/'.$node->nid,$wwoptions);
                            $mod = preg_replace("/% $lt\b/",$node->url,$mod);
							
                        }

                        if( $i == 0 ) /* let's try to add an edit link */
                        {
							$wwoptions['attributes']['name'] = $lt;
							$wwoptions['attributes']['class'] = 'wikinew';
                            $newnodeurl = l($lt,'node/add',$wwoptions);
							$mod = preg_replace("/% $lt\b/",$newnodeurl,$mod);
                        }
                    }
                    return $mod;
                case 'settings':
                    return $form;
            }
            break;
     }


 } // function twikifilter_filter


 /*
 * _twikifilter_get_node_term  - we need to use something custom instead of taxonomy_node_get_terms
 * @param nid node id
 * @return text text to return (or 0 for null)
 */
 function _twikifilter_get_node_term($nid)
 {
	$myweb = 0;
	if ( is_numeric($nid) )
	{
		$res = db_query("select tn.tid, td.name from term_node tn, term_data td, vocabulary v where tn.tid = td.tid and td.vid = v.vid and v.name = 'Webs' and tn.nid = %d",$nid);
		while ($term = db_fetch_object($res) )
			$myweb = $term->name; // let's hope there's only one web!
	}

	return $myweb;

 } // function _twikifilter_get_node_term

 /* 
 * _twikifilter_get_node - get a specific node by title and Webs term
 * @param title node title
 * @param term termdata name
 * @return array node array
 */
 function _twikifilter_get_node($title,$term)
 {
	$res = db_query("SELECT n.nid, n.title FROM {node} n, {term_node} tn, {term_data} td, {vocabulary} v WHERE n.nid = tn.nid and tn.tid = td.tid and v.vid = td.vid and v.name = 'Webs' and td.name = '%s' and  n.title = '%s'", $term, $title);
	
	while ( $node = db_fetch_object($res) ) // i think this is okay
		return $node;
	return 0;
 } // function _twikifilter_get_node


 function _twikifilter_make_child($children,$curdepth,$depth,$data)
 {
     if ($depth-$curdepth > 1)
     {
         if (isset($children['attributes']['index']))
         {
             $i = $children['attributes']['index'];
             $children[$i]['children'] = _twikifilter_make_child($children[$i]['data'],$curdepth+1,$depth,$data);
         }
         else
            $children['children'] = _twikifilter_make_child($children,$curdepth+1,$depth,$data);
     }
     else
     {
         if (isset($children['attributes']['index']))
         {
             $i = $chidren['attributes']['index']+1;
             $children[$i]['data'] = $data;
             $children['attributes']['index'] = $i++;
         }
         else
         {
            $children = array(0 => array('data' => $data));
            $children['attributes']['index'] = 0;
         }
     }

     return $children;
 } // function _twikifilter_make_child


 /**
  * _twikifilter_strip_array
  * This is the stupidest function ever to complement the world's stupidest theme function, theme_item_list()
  */
 function _twikifilter_strip_array(&$arr)
 {
     foreach ($arr as $key => $item)
     {
         if (strcmp($key,'attributes') == 0) 
             unset($arr[$key]);
         else
             if (is_array($arr[$key]))
                 _twikifilter_strip_array($arr[$key]);
     }
 } // function _twikifilter_strip_array
